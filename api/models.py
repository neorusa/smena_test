# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.urls import reverse

CHECK_TYPES = (
        ('kitchen', 'Kitchen'),
        ('client', 'Client'),
    )


class Printer(models.Model):
    name = models.CharField(max_length=200)
    api_key = models.CharField(max_length=64)
    check_type = models.CharField(max_length=20, choices=CHECK_TYPES, blank=True, default='kitchen')
    point_id = models.IntegerField(default=1)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('printer-detail', args=[str(self.id)])


class Check(models.Model):
    STATUSSES = (
        ('new', 'New'),
        ('rendered', 'Rendered'),
        ('printed', 'Printed'),
    )
    printer_id = models.ForeignKey(Printer, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=CHECK_TYPES, blank=True, default='kitchen')
    order = JSONField()
    status = models.CharField(max_length=20, choices=STATUSSES, blank=True, default='new')
    pdf_file = models.FileField(upload_to='pdf/')

    # Метод, чтобы получить список id чеков
    def json_new_checks(self):
        return {'id': self.id}

    def __str__(self):
        return self.id


