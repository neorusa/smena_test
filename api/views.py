from django.template.loader import render_to_string
import json
from django.http import HttpResponse, JsonResponse, FileResponse
from api.models import Check, Printer
from django.views.decorators.csrf import csrf_exempt
import django_rq
from api.jobs import generate_pdf


@csrf_exempt
def create_checks(request):
    """
    Создание чеков для печати на принтерах.
    :param request: Запрос от ERP
    :return: JSON:
                {"ok": "<Success message>"} - задача выполнилась успешно
                {"error": "<Fail Message>"} - проблемы при выполнении задачи
    """
    if request.method == 'POST':
        data = json.loads(request.body)

        printers = Printer.objects.filter(point_id=data['point_id'])
        if len(printers) == 0:
            answer = {'error': 'Для данной точки не настроено ни одного принтера'}
            return JsonResponse(answer, status=400)

        checks = Check.objects.filter(order__id=data['id'])   # data['id'] - номер заказа в ERP
        if len(checks) > 0:
            answer = {'error': 'Для данного заказа уже созданы чеки'}
            return JsonResponse(answer, status=400)

        for printer in printers:
            check = Check(printer_id=printer, type=printer.check_type, order=data, status='new')
            check.save()

            template = 'api/{}_check.html'.format(printer.check_type)
            html = render_to_string(template, data)

            django_rq.enqueue(generate_pdf, args=(html, check.id, data['id']))

        answer = {'ok': 'Чеки успешно созданы'}
        return JsonResponse(answer)
    else:
        return HttpResponse(status=404)


def new_checks(request):
    """
    Получаем список новых чеков для принтера
    :param request: Содержит api_key принтера, для которого надо получить список новых чеков
    :return: JSON: Список ID чеков, кторые были отренедерены, но не напечатаны.
    """
    data = request.GET
    try:
        printer = Printer.objects.get(api_key=data['api_key'])
    except Printer.DoesNotExist:
        answer = {'error': 'Не существует принтера с таким api_key'}
        return JsonResponse(answer, status=401)

    checks_list = [check.json_new_checks() for check in Check.objects.filter(printer_id=printer, status='rendered')]
    answer = {'checks': checks_list}

    return JsonResponse(answer)


def check(request):
    """
    Плучаем пдф файл для печати
    :param request: Содержит api_key принтера, для которого надо получить файл для печати и ID чека, который печатаем
    :return: PDF-file
    """
    data = request.GET
    try:
        printer = Printer.objects.get(api_key=data['api_key'])
    except Printer.DoesNotExist:
        answer = {'error': 'Не существует принтера с таким api_key'}
        return JsonResponse(answer, status=401)

    try:
        check_ = Check.objects.get(printer_id=printer, pk=data['check_id'])
    except Check.DoesNotExist:
        answer = {'error': 'Данного чека не существует'}
        return JsonResponse(answer, status=400)

    try:
        response = FileResponse(open(check_.pdf_file.path, 'rb'))
    except (FileExistsError, FileNotFoundError):
        answer = {'error': 'Для данного чека не сгенерирован PDF-файл'}
        return JsonResponse(answer, status=400)

    check_.status = 'printed'
    check_.save()

    return response
