# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
import json
import requests
from api.models import Check
import base64
import os
from django.conf import settings


def generate_pdf(html, check_id, order_id):
    """
    Асинхронная задача генерации pdf-файла чека.
    :param html: чек, созданнный в html
    :param check_id: ID чека в БД
    :param order_id: Номер заказа из ERP
    :return: void
    """
    # Адрес сервиса wkhtmltopdf
    url = 'http://127.0.0.1:80/'

    # Создадим запрос к wkhtmltopdf
    data = {
        'contents': base64.b64encode(bytes(html, 'utf-8')).decode('utf-8')
    }
    headers = {
        'Content-Type': 'application/json',
    }

    # Получаем сгенерированный pdf
    response = requests.post(url, data=json.dumps(data), headers=headers)

    check = Check.objects.get(pk=check_id)

    # Сгенерируем название файла и запишем его в ФС
    filename = 'pdf/' + str(order_id) + '_' + str(check.type) + '.pdf'
    path_to_file = os.path.join(settings.MEDIA_ROOT, filename)
    with open(path_to_file, 'wb') as f:
        f.write(response.content)

    check.status = 'rendered'
    check.pdf_file = filename
    check.save()
