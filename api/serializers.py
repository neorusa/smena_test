# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)

from rest_framework import serializers
from api.models import Printer, Check


class CheckSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    printer_id = serializers.IntegerField(read_only=True)
